import {
  getFileFromMap,
  getMapFromFile,
  readFile,
  writeToFile,
} from "./utils/fileUtils";
import { simulateAdventure } from "./functions/simulateAdventure";

(function main() {
  const filePathInput = process.argv[2];
  const filePathOutput = process.argv[3];

  const fileInput = readFile(filePathInput);
  if (fileInput === null) {
    throw new Error("Erreur lecture fichier");
  }
  const map = getMapFromFile(fileInput);
  const newMap = simulateAdventure(map);
  const fileOutput = getFileFromMap(newMap);
  writeToFile(filePathOutput, fileOutput);
})();
