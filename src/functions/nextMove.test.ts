import { newOrientation, newPositions } from "../functions/adventurerMouvement";

describe("newPositions function", () => {
  test("should move adventurer forward when 'A' is provided", () => {
    const adventurer = {
      horizontal: 0,
      vertical: 1,
      orientation: "N",
      name: "",
      moves: [],
      numberTreasures: 0,
    };

    expect(newPositions(adventurer)).toEqual({
      ...adventurer,
      vertical: 0,
    });
  });

  test("should change adventurer's orientation to the right when 'D' is provided", () => {
    const adventurer = {
      horizontal: 0,
      vertical: 0,
      orientation: "N",
      name: "",
      moves: [],
      numberTreasures: 0,
    };

    expect(newOrientation(adventurer, "D")).toEqual({
      ...adventurer,
      orientation: "E",
    });
  });

  test("should change adventurer's orientation to the left when 'G' is provided", () => {
    const adventurer = {
      horizontal: 0,
      vertical: 0,
      orientation: "N",
      name: "",
      moves: [],
      numberTreasures: 0,
    };

    expect(newOrientation(adventurer, "G")).toEqual({
      ...adventurer,
      orientation: "O",
    });
  });

  test("should throw an error for an unknown movement", () => {
    const adventurer = {
      horizontal: 0,
      vertical: 0,
      orientation: "N",
      name: "",
      moves: [],
      numberTreasures: 0,
    };
    expect(() => newOrientation(adventurer, "X")).toThrow(
      "Mouvement non reconnu"
    );
  });
});
