import { simulateAdventure } from "../functions/simulateAdventure";

describe("simulateAdventure", () => {
  test("devrait simuler l'aventure avec succès", () => {
    const mapTreasures = {
      map: { width: 3, length: 4 },
      montains: [],
      treasures: [{ horizontal: 1, vertical: 2, numberTreasures: 1 }],
      adventurers: [
        {
          name: "Lara",
          horizontal: 1,
          vertical: 1,
          orientation: "S",
          moves: ["A", "A"],
          numberTreasures: 0,
        },
        {
          name: "Indiana",
          horizontal: 2,
          vertical: 2,
          orientation: "E",
          moves: ["D", "A"],
          numberTreasures: 0,
        },
      ],
    };

    const result = simulateAdventure(mapTreasures);

    expect(result.map).toEqual({ width: 3, length: 4 });
    expect(result.montains).toEqual([]);
    expect(result.treasures).toEqual([
      { horizontal: 1, vertical: 2, numberTreasures: 0 },
    ]);
    expect(result.adventurers).toEqual([
      {
        name: "Lara",
        horizontal: 1,
        vertical: 3,
        orientation: "S",
        moves: [],
        numberTreasures: 1,
      },

      {
        name: "Indiana",
        horizontal: 2,
        vertical: 3,
        orientation: "S",
        moves: [],
        numberTreasures: 0,
      },
    ]);
  });

  test("devrait ne rien faire si aucun aventurier n'a de mouvement", () => {
    const mapTreasures = {
      map: { width: 3, length: 4 },
      montains: [],
      treasures: [{ horizontal: 1, vertical: 1, numberTreasures: 1 }],
      adventurers: [
        {
          name: "Lara",
          horizontal: 1,
          vertical: 1,
          orientation: "S",
          moves: [],
          numberTreasures: 0,
        },
        {
          name: "Indiana",
          horizontal: 2,
          vertical: 2,
          orientation: "E",
          moves: [],
          numberTreasures: 0,
        },
      ],
    };

    const result = simulateAdventure(mapTreasures);

    expect(result.map).toEqual({ width: 3, length: 4 });
    expect(result.montains).toEqual([]);
    expect(result.treasures).toEqual([
      { horizontal: 1, vertical: 1, numberTreasures: 1 },
    ]);
    expect(result.adventurers).toEqual([
      {
        name: "Lara",
        horizontal: 1,
        vertical: 1,
        orientation: "S",
        moves: [],
        numberTreasures: 0,
      },
      {
        name: "Indiana",
        horizontal: 2,
        vertical: 2,
        orientation: "E",
        moves: [],
        numberTreasures: 0,
      },
    ]);
  });
});
