import { decreaseTresors } from "../functions/adventurerMouvement";

// Teste la fonction decreaseTresors
describe("decreaseTresors function", () => {
  test("should decrease the number of a tresor when coordinates match", () => {
    const tresors = [
      { horizontal: 2, vertical: 3, numberTreasures: 5 },
      { horizontal: 4, vertical: 1, numberTreasures: 3 },
    ];

    expect(decreaseTresors(tresors, 2, 3)[0].numberTreasures).toBe(4);
  });

  test("should not change the number of any tresor when coordinates do not match", () => {
    const tresors = [
      { horizontal: 2, vertical: 3, numberTreasures: 5 },
      { horizontal: 4, vertical: 1, numberTreasures: 3 },
    ];

    decreaseTresors(tresors, 1, 1);

    expect(decreaseTresors(tresors, 1, 1)[0].numberTreasures).toBe(5);
    expect(decreaseTresors(tresors, 1, 1)[1].numberTreasures).toBe(3);
  });

  test("should not change the number of any tresor when tresors array is empty", () => {
    const tresors = [] as Treasure[];

    decreaseTresors(tresors, 3, 4);

    expect(decreaseTresors(tresors, 3, 4)).toEqual([]);
  });
});
