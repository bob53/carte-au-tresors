import { shifting } from "./adventurerMouvement";

/**
 * Simule l'aventure en effectuant les déplacements des aventuriers sur la carte jusqu'à ce que tous les mouvements soient épuisés.
 *
 * @param {MapTreasures} map - La carte initiale avec les trésors et les aventuriers.
 * @returns {MapTreasures} - La carte mise à jour après la simulation de l'aventure.
 */
export const simulateAdventure = (map: MapTreasures): MapTreasures => {
  //Récupère le nombre max de mouvements comme condition d'arrêt
  const maxMoves = map.adventurers.reduce((max, adventurer) => {
    const currentSize = adventurer.moves.length;
    return currentSize > max ? currentSize : max;
  }, 0);

  let newMap = { ...map };

  for (let step = 0; step < maxMoves; step++) {
    const newAdventurers = newMap.adventurers.map((adventurer) => {
      if (adventurer.moves.length !== 0) {
        const { newTreasures, newAdventurer } = shifting(newMap, adventurer);
        newMap = { ...newMap, treasures: newTreasures };
        return newAdventurer;
      }
      return adventurer;
    });

    newMap = { ...newMap, adventurers: newAdventurers };
  }

  return newMap;
};
