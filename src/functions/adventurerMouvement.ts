import { canMove, isTresor } from "../validations/mapValidation";

/**
 * Diminue le nombre de trésors d'une case spécifiée.
 *
 * @param {Treasure[] | undefined} treasures - Tableau de trésors ou undefined.
 * @param {number} x - Coordonnée horizontale de la case.
 * @param {number} y - Coordonnée verticale de la case.
 * @returns {Treasure[]} - Un nouveau tableau de trésors avec la diminution du nombre de trésors sur la case spécifiée.
 */
const decreaseTresors = (
  treasures: Treasure[] | undefined,
  x: number,
  y: number
): Treasure[] => {
  return (
    treasures?.map((treasure) => {
      if (
        treasure.horizontal === x &&
        treasure.vertical === y &&
        treasure.numberTreasures !== 0
      ) {
        return { ...treasure, numberTreasures: treasure.numberTreasures - 1 };
      }
      return treasure;
    }) ?? []
  );
};

/**
 * Calcule et renvoie la nouvelle position de l'aventurier en fonction de son orientation actuelle.
 *
 * @param {Adventurer} adventurer - L'objet aventurier contenant les informations sur sa position et son orientation.
 * @returns {Adventurer} - Un nouvel objet aventurier avec la nouvelle position calculée en fonction de l'orientation.
 * @throws {Error} - Lance une erreur si l'orientation n'est pas reconnue.
 */
const newPositions = (adventurer: Adventurer): Adventurer => {
  const { orientation, horizontal, vertical } = adventurer;

  switch (orientation) {
    case "N":
      return { ...adventurer, vertical: vertical - 1 };

    case "S":
      return { ...adventurer, vertical: vertical + 1 };

    case "E":
      return { ...adventurer, horizontal: horizontal + 1 };

    case "O":
      return { ...adventurer, horizontal: horizontal - 1 };

    default:
      throw new Error("Orientation non reconnue");
  }
};

/**
 * Calcule et renvoie la nouvelle orientation de l'aventurier en fonction du mouvement spécifié.
 *
 * @param {Adventurer} adventurer - L'objet aventurier contenant les informations sur son orientation actuelle.
 * @param {string | undefined} move - Le mouvement à effectuer ("D" pour droite, "G" pour gauche).
 * @returns {Adventurer} - Un nouvel objet aventurier avec la nouvelle orientation calculée en fonction du mouvement.
 * @throws {Error} - Lance une erreur si le mouvement n'est pas reconnu.
 */
const newOrientation = (
  adventurer: Adventurer,
  move: string | undefined
): Adventurer => {
  const { orientation } = adventurer;

  switch (move) {
    case "D":
      return {
        ...adventurer,
        orientation: getNextOrientation(orientation, "D"),
      };

    case "G":
      return {
        ...adventurer,
        orientation: getNextOrientation(orientation, "G"),
      };

    default:
      throw new Error("Mouvement non reconnu");
  }
};

/**
 * Renvoie la nouvelle orientation basée sur le mouvement spécifié (D pour droite, G pour gauche).
 *
 * @param {string} currentOrientation - L'orientation actuelle de l'aventurier.
 * @param {string} move - Le mouvement à effectuer ("D" pour droite, "G" pour gauche).
 * @returns {string} - La nouvelle orientation calculée en fonction du mouvement.
 * @throws {Error} - Lance une erreur si l'orientation actuelle n'est pas reconnue.
 */
const getNextOrientation = (
  currentOrientation: string,
  move: string
): string => {
  const directions = ["N", "E", "S", "O"];
  const currentIndex = directions.indexOf(currentOrientation);

  if (currentIndex === -1) {
    throw new Error("Orientation non reconnue");
  }

  //Aventurier tourne vers la gauche ou la droite
  const increment = move === "D" ? 1 : -1;
  const nextIndex =
    (currentIndex + directions.length + increment) % directions.length;

  return directions[nextIndex];
};

/**
 * Effectue le déplacement de l'aventurier sur la carte et renvoie la nouvelle position.
 *
 * @param {MapTreasures} map - La carte avec les trésors.
 * @param {Adventurer} adventurer - L'objet aventurier contenant les informations sur sa position et son orientation.
 * @returns {{ newTreasures: Treasure[]; newAdventurer: Adventurer }} - la nouvelle position de l'aventurier.
 */
const changePosition = (
  map: MapTreasures,
  adventurer: Adventurer
): { newTreasures: Treasure[]; newAdventurer: Adventurer } => {
  const intermediateAdventurer = newPositions(adventurer);

  // Vérifie que la nouvelle position est une position valide
  if (canMove(map, intermediateAdventurer)) {
    return {
      newTreasures: [...map.treasures],
      newAdventurer: { ...intermediateAdventurer },
    };
  }

  return { newTreasures: [...map.treasures], newAdventurer: { ...adventurer } };
};

/**
 * Modifie l'orientation de l'aventurier et renvoie la nouvelle orientation.
 *
 * @param {MapTreasures} map - La carte avec les trésors.
 * @param {Adventurer} adventurer - L'objet aventurier contenant les informations sur sa position et son orientation.
 * @param {string | undefined} move - Le mouvement à effectuer ("D" pour droite, "G" pour gauche).
 * @returns {{ newTreasures: Treasure[]; newAdventurer: Adventurer }} - la nouvelle orientation.
 */
const changeOrientation = (
  map: MapTreasures,
  adventurer: Adventurer,
  move: string | undefined
): { newTreasures: Treasure[]; newAdventurer: Adventurer } => {
  const intermediateAdventurer = newOrientation(adventurer, move);
  return {
    newTreasures: [...map.treasures],
    newAdventurer: { ...intermediateAdventurer },
  };
};

/**
 * Effectue le déplacement ou la modification de l'orientation de l'aventurier en fonction du prochain mouvement dans la liste.
 *
 * @param {MapTreasures} map - La carte avec les trésors.
 * @param {Adventurer} adventurer - L'objet aventurier contenant les informations sur sa position, son orientation et ses mouvements.
 * @returns {{ newTreasures: Treasure[]; newAdventurer: Adventurer }} - Les nouvelles positions de l'aventurier et les trésors mis à jour.
 */
const shifting = (
  map: MapTreasures,
  adventurer: Adventurer
): { newTreasures: Treasure[]; newAdventurer: Adventurer } => {
  const { moves } = adventurer;

  if (moves.length === 0) {
    return {
      newTreasures: [...map.treasures],
      newAdventurer: { ...adventurer },
    };
  }

  const move = moves.shift();

  if (move === "A") {
    const { newAdventurer, newTreasures } = changePosition(map, adventurer);
    const hasTresor = isTresor(
      map.treasures,
      newAdventurer.horizontal,
      newAdventurer.vertical
    );
    return {
      newTreasures: hasTresor
        ? //Décremente de 1 le nombre de trésors et augment de 1 celui de l'aventurier
          decreaseTresors(
            map.treasures,
            newAdventurer.horizontal,
            newAdventurer.vertical
          )
        : [...newTreasures],
      newAdventurer: {
        ...newAdventurer,
        numberTreasures: hasTresor
          ? adventurer.numberTreasures + 1
          : adventurer.numberTreasures,
      },
    };
  } else {
    return changeOrientation(map, adventurer, move);
  }
};

export {
  shifting,
  changeOrientation,
  changePosition,
  newOrientation,
  newPositions,
  decreaseTresors,
};
