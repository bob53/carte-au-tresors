type MapTreasures = {
  map: MapDimension;
  montains: Montaine[];
  treasures: Treasure[];
  adventurers: Adventurer[];
};

type MapDimension = {
  width: number;
  length: number;
};

type Montaine = {
  horizontal: number;
  vertical: number;
};

type Treasure = {
  horizontal: number;
  vertical: number;
  numberTreasures: number;
};

type Adventurer = {
  name: string;
  horizontal: number;
  vertical: number;
  orientation: string;
  moves: string[];
  numberTreasures: number;
};
