/**
 * Vérifie si la position spécifiée est en dehors des limites de la carte.
 *
 * @param {MapDimension} map - Les dimensions de la carte.
 * @param {number} x - Coordonnée horizontale.
 * @param {number} y - Coordonnée verticale.
 * @returns {boolean} - True si la position est en dehors des limites de la carte, sinon false.
 */
const isOutMap = (map: MapDimension, x: number, y: number): boolean => {
  return x < 0 || y < 0 || x >= map.width || y >= map.length;
};

/**
 * Vérifie si la position spécifiée est occupée par une montagne.
 *
 * @param {Montaine[] | undefined} montains - La liste des montagnes.
 * @param {number} x - Coordonnée horizontale.
 * @param {number} y - Coordonnée verticale.
 * @returns {boolean} - True si la position est occupée par une montagne, sinon false.
 */
const isMontaine = (
  montains: Montaine[] | undefined,
  x: number,
  y: number
): boolean => {
  return (
    montains?.some(
      (montain) => montain.horizontal === x && montain.vertical === y
    ) ?? false
  );
};

/**
 * Vérifie si la position spécifiée est occupée par un autre aventurier.
 *
 * @param {Adventurer[] | undefined} adventurers - La liste des aventuriers.
 * @param {number} x - Coordonnée horizontale.
 * @param {number} y - Coordonnée verticale.
 * @param {string} name - Nom de l'aventurier actuel.
 * @returns {boolean} - True si la position est occupée par un autre aventurier, sinon false.
 */
const isAdventurer = (
  adventurers: Adventurer[] | undefined,
  x: number,
  y: number,
  name: string
): boolean => {
  return (
    adventurers?.some(
      (adventurer) =>
        adventurer.horizontal === x &&
        adventurer.vertical === y &&
        adventurer.name !== name
    ) ?? false
  );
};

/**
 * Vérifie si l'aventurier peut se déplacer à la position spécifiée sur la carte.
 *
 * @param {MapTreasures} map - La carte avec les trésors, les montagnes et les aventuriers.
 * @param {Adventurer} adventurer - L'aventurier dont la position doit être vérifiée.
 * @returns {boolean} - True si l'aventurier peut se déplacer à la position spécifiée, sinon false.
 */
const canMove = (map: MapTreasures, adventurer: Adventurer): boolean => {
  const { horizontal, vertical, name } = adventurer;
  return !(
    isOutMap(map.map, horizontal, vertical) ||
    isMontaine(map.montains, horizontal, vertical) ||
    isAdventurer(map.adventurers, horizontal, vertical, name)
  );
};

/**
 * Vérifie si la case spécifiée contient un trésor disponible.
 *
 * @param {Treasure[]} treasures - La liste des trésors sur la carte.
 * @param {number} x - Coordonnée horizontale.
 * @param {number} y - Coordonnée verticale.
 * @returns {boolean} - True si la case contient un trésor disponible, sinon false.
 */
const isTresor = (treasures: Treasure[], x: number, y: number): boolean => {
  return treasures.some(
    (treasure) =>
      treasure.horizontal === x &&
      treasure.vertical === y &&
      treasure.numberTreasures > 0
  );
};

export { isAdventurer, isMontaine, isOutMap, isTresor, canMove };
