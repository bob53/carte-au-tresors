import * as fs from "fs";

/**
 * Lit le contenu d'un fichier et le retourne sours forme de chaîne de caractères.
 *
 * @param {string} filePath - Le chemin du fichier à lire.
 * @returns {string | null} - Le contenu du fichier lu en tant que chaîne de caractères, ou null en cas d'erreur.
 */
const readFile = (filePath: string): string | null => {
  try {
    const content = fs.readFileSync(filePath, "utf-8");
    return content;
  } catch (error) {
    console.error(
      `Une erreur s'est produite lors de la lecture du fichier : ${error}`
    );
    return null;
  }
};

/**
 * Écrit le contenu dans un fichier.
 *
 * @param {string} fileName - Le nom du fichier à écrire.
 * @param {string} content - Le contenu à écrire dans le fichier.
 * @returns {void}
 */
const writeToFile = (fileName: string, content: string): void => {
  try {
    fs.writeFileSync(fileName, content, "utf-8");
    console.log(
      `Le contenu a été écrit avec succès dans le fichier ${fileName}`
    );
  } catch (error) {
    console.error(
      `Une erreur s'est produite lors de l'écriture dans le fichier ${fileName} : ${error}`
    );
  }
};

/**
 * Extrait les informations de la carte et des éléments (montagnes, trésors, aventuriers) à partir d'une chaîne de caractères.
 *
 * @param {string} input - La chaîne de caractères représentant la carte et ses éléments.
 * @returns {MapTreasures} - Les informations extraites de la chaîne.
 * @throws {Error} - Lance une erreur si aucune carte n'est trouvée.
 */
const getMapFromFile = (input: string): MapTreasures => {
  const lines = input.split("\n");
  let map: MapDimension | null = null;
  const montains: Montaine[] = [];
  const treasures: Treasure[] = [];
  const adventurers: Adventurer[] = [];

  for (const line of lines) {
    // Ignorer les lignes commençant par '#'
    if (line.startsWith("#")) {
      continue;
    }

    const parts = line.split(" - ");

    switch (parts[0]) {
      case "C":
        map = {
          width: parseInt(parts[1]),
          length: parseInt(parts[2]),
        };
        break;
      case "M":
        montains.push({
          horizontal: parseInt(parts[1]),
          vertical: parseInt(parts[2]),
        });
        break;
      case "T":
        treasures.push({
          horizontal: parseInt(parts[1]),
          vertical: parseInt(parts[2]),
          numberTreasures: parseInt(parts[3]),
        });
        break;
      case "A":
        adventurers.push({
          name: parts[1],
          horizontal: parseInt(parts[2]),
          vertical: parseInt(parts[3]),
          orientation: parts[4],
          moves: parts[5].split(""),
          numberTreasures: 0,
        });
        break;
      default:
        // Ignorer les lignes avec des formats non reconnus
        break;
    }
  }

  if (!map) {
    throw new Error("Aucune carte trouvée.");
  }

  return { map, montains, treasures, adventurers };
};

/**
 * Génère une chaîne de caractères à partir des informations de la carte et des éléments (montagnes, trésors, aventuriers).
 *
 * @param {MapTreasures} mapTreasures - Les informations de la carte et des éléments.
 * @returns {string} - La chaîne de caractères générée.
 */
const getFileFromMap = (mapTreasures: MapTreasures): string => {
  const lines: string[] = [];

  // Carte
  lines.push(`C - ${mapTreasures.map.width} - ${mapTreasures.map.length}`);

  // Montagnes
  mapTreasures.montains.forEach((montain) => {
    lines.push(`M - ${montain.horizontal} - ${montain.vertical}`);
  });

  // Trésors
  mapTreasures.treasures
    .filter((treasure) => treasure.numberTreasures > 0)
    .forEach((treasure) => {
      lines.push(
        `T - ${treasure.horizontal} - ${treasure.vertical} - ${treasure.numberTreasures}`
      );
    });

  // Aventuriers
  mapTreasures.adventurers.forEach((adventurer) => {
    lines.push(
      `A - ${adventurer.name} - ${adventurer.horizontal} - ${adventurer.vertical} - ${adventurer.orientation} - ${adventurer.numberTreasures}`
    );
  });

  return lines.join("\n");
};

export { readFile, writeToFile, getFileFromMap, getMapFromFile };
