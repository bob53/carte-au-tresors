"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const adventurerMouvement_1 = require("../functions/adventurerMouvement");
describe("changePosition", () => {
    test("devrait avancer l'aventurier ", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [{ horizontal: 1, vertical: 1, numberTreasures: 1 }],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 0,
                    orientation: "S",
                    moves: [],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        const result = (0, adventurerMouvement_1.changePosition)(mapTreasures, adventurer);
        expect(result.newTreasures).toEqual([
            { horizontal: 1, vertical: 1, numberTreasures: 1 },
        ]);
        expect(result.newAdventurer).toEqual({
            name: "Lara",
            horizontal: 1,
            vertical: 1,
            orientation: "S",
            moves: [],
            numberTreasures: 0,
        });
    });
    test("devrait ne pas avancer si le déplacement n'est pas possible", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 3,
                    orientation: "S",
                    moves: [],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        const result = (0, adventurerMouvement_1.changePosition)(mapTreasures, adventurer);
        expect(result.newTreasures).toEqual([]);
        expect(result.newAdventurer).toEqual({
            name: "Lara",
            horizontal: 1,
            vertical: 3,
            orientation: "S",
            moves: [],
            numberTreasures: 0,
        });
    });
});
describe("changeOrientation", () => {
    test("devrait changer l'orientation de l'aventurier", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 1,
                    orientation: "S",
                    moves: [],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        const result = (0, adventurerMouvement_1.changeOrientation)(mapTreasures, adventurer, "D");
        expect(result.newTreasures).toEqual([]);
        expect(result.newAdventurer).toEqual({
            name: "Lara",
            horizontal: 1,
            vertical: 1,
            orientation: "O", // Orientation changée vers l'Ouest après un mouvement 'D'.
            moves: [],
            numberTreasures: 0,
        });
    });
    test("devrait ne pas changer l'orientation si le mouvement n'est pas valide", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 1,
                    orientation: "S",
                    moves: [],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        expect(() => (0, adventurerMouvement_1.changeOrientation)(mapTreasures, adventurer, "X")).toThrow("Mouvement non reconnu");
    });
});
describe("shifting", () => {
    test('devrait avancer l\'aventurier si le prochain mouvement est "A"', () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 1,
                    orientation: "S",
                    moves: ["A"],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        const result = (0, adventurerMouvement_1.shifting)(mapTreasures, adventurer);
        expect(result.newTreasures).toEqual([]);
        expect(result.newAdventurer).toEqual({
            name: "Lara",
            horizontal: 1,
            vertical: 2,
            orientation: "S",
            moves: [],
            numberTreasures: 0,
        });
    });
    test("devrait changer l'orientation de l'aventurier si le prochain mouvement n'est pas \"A\"", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 1,
                    orientation: "S",
                    moves: ["D"],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        const result = (0, adventurerMouvement_1.shifting)(mapTreasures, adventurer);
        expect(result.newTreasures).toEqual([]);
        expect(result.newAdventurer).toEqual({
            name: "Lara",
            horizontal: 1,
            vertical: 1,
            orientation: "O",
            moves: [],
            numberTreasures: 0,
        });
    });
    test("devrait ne rien faire si il n'y a plus de mouvement", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [],
            treasures: [],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 1,
                    orientation: "S",
                    moves: [],
                    numberTreasures: 0,
                },
            ],
        };
        const adventurer = mapTreasures.adventurers[0];
        const result = (0, adventurerMouvement_1.shifting)(mapTreasures, adventurer);
        expect(result.newTreasures).toEqual([]);
        expect(result.newAdventurer).toEqual({
            name: "Lara",
            horizontal: 1,
            vertical: 1,
            orientation: "S",
            moves: [],
            numberTreasures: 0,
        });
    });
});
