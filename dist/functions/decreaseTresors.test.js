"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// Importe la fonction à tester
const adventurerMouvement_1 = require("../functions/adventurerMouvement");
// Teste la fonction decreaseTresors
describe("decreaseTresors function", () => {
    test("should decrease the number of a tresor when coordinates match", () => {
        const tresors = [
            { horizontal: 2, vertical: 3, numberTreasures: 5 },
            { horizontal: 4, vertical: 1, numberTreasures: 3 },
        ];
        // Vérifie que le nombre du trésor a été diminué
        expect((0, adventurerMouvement_1.decreaseTresors)(tresors, 2, 3)[0].numberTreasures).toBe(4);
    });
    test("should not change the number of any tresor when coordinates do not match", () => {
        const tresors = [
            { horizontal: 2, vertical: 3, numberTreasures: 5 },
            { horizontal: 4, vertical: 1, numberTreasures: 3 },
        ];
        (0, adventurerMouvement_1.decreaseTresors)(tresors, 1, 1);
        // Vérifie que le nombre des trésors n'a pas changé
        expect((0, adventurerMouvement_1.decreaseTresors)(tresors, 1, 1)[0].numberTreasures).toBe(5);
        expect((0, adventurerMouvement_1.decreaseTresors)(tresors, 1, 1)[1].numberTreasures).toBe(3);
    });
    test("should not change the number of any tresor when tresors array is empty", () => {
        const tresors = [];
        (0, adventurerMouvement_1.decreaseTresors)(tresors, 3, 4);
        // Vérifie que le nombre des trésors reste vide
        expect((0, adventurerMouvement_1.decreaseTresors)(tresors, 3, 4)).toEqual([]);
    });
});
