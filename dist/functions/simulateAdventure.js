"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.simulateAdventure = void 0;
const adventurerMouvement_1 = require("./adventurerMouvement");
/**
 * Simule l'aventure en effectuant les déplacements des aventuriers sur la carte jusqu'à ce que tous les mouvements soient épuisés.
 *
 * @param {MapTreasures} map - La carte initiale avec les trésors et les aventuriers.
 * @returns {MapTreasures} - La carte mise à jour après la simulation de l'aventure.
 */
const simulateAdventure = (map) => {
    //Récupère le nombre max de mouvements comme condition d'arrêt
    const maxMoves = map.adventurers.reduce((max, adventurer) => {
        const currentSize = adventurer.moves.length;
        return currentSize > max ? currentSize : max;
    }, 0);
    let newMap = { ...map };
    for (let step = 0; step < maxMoves; step++) {
        const newAdventurers = newMap.adventurers.map((adventurer) => {
            if (adventurer.moves.length !== 0) {
                const { newTreasures, newAdventurer } = (0, adventurerMouvement_1.shifting)(newMap, adventurer);
                newMap = { ...newMap, treasures: newTreasures };
                return newAdventurer;
            }
            return adventurer;
        });
        newMap = { ...newMap, adventurers: newAdventurers };
    }
    return newMap;
};
exports.simulateAdventure = simulateAdventure;
