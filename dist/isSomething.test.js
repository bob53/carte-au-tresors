"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("./main");
// Teste la fonction isOutMap
describe("isOutMap function", () => {
    test("should return true if x is greater than carte.width", () => {
        const carte = { width: 5, length: 5 };
        expect((0, main_1.isOutMap)(carte, 6, 3)).toBe(true);
    });
    test("should return true if y is greater than carte.length", () => {
        const carte = { width: 5, length: 5 };
        expect((0, main_1.isOutMap)(carte, 3, 6)).toBe(true);
    });
    test("should return false if x and y are within the map boundaries", () => {
        const carte = { width: 5, length: 5 };
        expect((0, main_1.isOutMap)(carte, 3, 3)).toBe(false);
    });
});
// Teste la fonction isMontagne
describe("isMontagne function", () => {
    test("should return true if x and y match a montagne's coordinates", () => {
        const montagnes = [
            { horizontal: 2, vertical: 3 },
            { horizontal: 4, vertical: 1 },
        ];
        expect((0, main_1.isMontaine)(montagnes, 2, 3)).toBe(true);
    });
    test("should return false if x and y do not match any montagne's coordinates", () => {
        const montagnes = [
            { horizontal: 2, vertical: 3 },
            { horizontal: 4, vertical: 1 },
        ];
        expect((0, main_1.isMontaine)(montagnes, 1, 1)).toBe(false);
    });
});
// Teste la fonction isAventurier
describe("isAventurier function", () => {
    test("should return true if x and y match an aventurier's coordinates", () => {
        const aventuriers = [
            { horizontal: 1, vertical: 4, name: "toto" },
            { horizontal: 3, vertical: 2, name: "tata" },
        ];
        expect((0, main_1.isAdventurer)(aventuriers, 3, 2, "Lara")).toBe(true);
    });
    test("should return false if x and y do not match any aventurier's coordinates", () => {
        const aventuriers = [
            {
                horizontal: 1,
                vertical: 4,
                name: "toto",
            },
            {
                horizontal: 3,
                vertical: 2,
                name: "tata",
            },
        ];
        expect((0, main_1.isAdventurer)(aventuriers, 5, 5, "Lara")).toBe(false);
    });
});
describe("isAventurier function", () => {
    test("should return false if it's the same adventurer", () => {
        const aventuriers = [
            { horizontal: 1, vertical: 4, name: "toto" },
            { horizontal: 3, vertical: 2, name: "Lara" },
        ];
        expect((0, main_1.isAdventurer)(aventuriers, 3, 2, "Lara")).toBe(false);
    });
    test("should return false if x and y do not match any aventurier's coordinates", () => {
        const aventuriers = [
            {
                horizontal: 1,
                vertical: 4,
                name: "toto",
            },
            {
                horizontal: 3,
                vertical: 2,
                name: "tata",
            },
        ];
        expect((0, main_1.isAdventurer)(aventuriers, 5, 5, "Lara")).toBe(false);
    });
});
// Teste la fonction isTresor
describe("isTresor function", () => {
    test("should return true if x and y match a tresor's coordinates", () => {
        const tresors = [
            { horizontal: 2, vertical: 3, numberTreasures: 1 },
            { horizontal: 4, vertical: 1, numberTreasures: 1 },
        ];
        expect((0, main_1.isTresor)(tresors, 2, 3)).toBe(true);
    });
    test("should return false if x and y do not match any tresor's coordinates", () => {
        const tresors = [
            { horizontal: 2, vertical: 3, numberTreasures: 1 },
            { horizontal: 4, vertical: 1, numberTreasures: 1 },
        ];
        expect((0, main_1.isTresor)(tresors, 1, 1)).toBe(false);
    });
    test("should return false if tresors array is empty", () => {
        const tresors = [];
        expect((0, main_1.isTresor)(tresors, 3, 4)).toBe(false);
    });
});
