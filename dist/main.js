"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fileUtils_1 = require("./utils/fileUtils");
const simulateAdventure_1 = require("./functions/simulateAdventure");
(function main() {
    const filePathInput = process.argv[2];
    const filePathOutput = process.argv[3];
    const fileInput = (0, fileUtils_1.readFile)(filePathInput);
    if (fileInput === null) {
        throw new Error("Erreur lecture fichier");
    }
    const map = (0, fileUtils_1.getMapFromFile)(fileInput);
    const newMap = (0, simulateAdventure_1.simulateAdventure)(map);
    const fileOutput = (0, fileUtils_1.getFileFromMap)(newMap);
    (0, fileUtils_1.writeToFile)(filePathOutput, fileOutput);
})();
