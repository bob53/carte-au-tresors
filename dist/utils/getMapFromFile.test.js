"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fileUtils_1 = require("../utils/fileUtils");
describe("getMapFromFile", () => {
    test("devrait retourner un objet MapTreasures avec une carte valide", () => {
        const input = `C - 3 - 4
# Ignorer cette ligne
M - 1 - 0
T - 0 - 3 - 2
A - Lara - 1 - 1 - S - AADADAGGA`;
        const result = (0, fileUtils_1.getMapFromFile)(input);
        expect(result.map).toEqual({ width: 3, length: 4 });
        expect(result.montains).toEqual([{ horizontal: 1, vertical: 0 }]);
        expect(result.treasures).toEqual([
            { horizontal: 0, vertical: 3, numberTreasures: 2 },
        ]);
        expect(result.adventurers).toEqual([
            {
                name: "Lara",
                horizontal: 1,
                vertical: 1,
                orientation: "S",
                moves: ["A", "A", "D", "A", "D", "A", "G", "G", "A"],
                numberTreasures: 0,
            },
        ]);
    });
    test('devrait ignorer les lignes commençant par "#"', () => {
        const input = `# Ignorer cette ligne
C - 5 - 5
M - 2 - 2
T - 1 - 1 - 3`;
        const result = (0, fileUtils_1.getMapFromFile)(input);
        expect(result.map).toEqual({ width: 5, length: 5 });
        expect(result.montains).toEqual([{ horizontal: 2, vertical: 2 }]);
        expect(result.treasures).toEqual([
            { horizontal: 1, vertical: 1, numberTreasures: 3 },
        ]);
        expect(result.adventurers).toEqual([]);
    });
    test("devrait lever une erreur si aucune carte n'est trouvée", () => {
        const input = `M - 1 - 0
T - 0 - 3 - 2
A - Lara - 1 - 1 - S - AADADAGGA`;
        expect(() => (0, fileUtils_1.getMapFromFile)(input)).toThrow("Aucune carte trouvée.");
    });
});
