"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fileUtils_1 = require("../utils/fileUtils");
describe("getFileFromMap", () => {
    test("devrait retourner une chaîne de caractères formattée avec une carte valide", () => {
        const mapTreasures = {
            map: { width: 3, length: 4 },
            montains: [{ horizontal: 1, vertical: 0 }],
            treasures: [{ horizontal: 0, vertical: 3, numberTreasures: 2 }],
            adventurers: [
                {
                    name: "Lara",
                    horizontal: 1,
                    vertical: 1,
                    orientation: "S",
                    moves: ["A", "A", "D", "A", "D", "A", "G", "G", "A"],
                    numberTreasures: 0,
                },
            ],
        };
        const result = (0, fileUtils_1.getFileFromMap)(mapTreasures);
        expect(result).toEqual(`C - 3 - 4
M - 1 - 0
T - 0 - 3 - 2
A - Lara - 1 - 1 - S - 0`);
    });
    test("devrait exclure les trésors avec numberTreasures à 0", () => {
        const mapTreasures = {
            map: { width: 5, length: 5 },
            montains: [{ horizontal: 2, vertical: 2 }],
            treasures: [{ horizontal: 1, vertical: 1, numberTreasures: 0 }],
            adventurers: [],
        };
        const result = (0, fileUtils_1.getFileFromMap)(mapTreasures);
        expect(result).toEqual(`C - 5 - 5
M - 2 - 2`);
    });
});
