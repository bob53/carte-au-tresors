# Projet de Simulation d'Aventure sur Carte

Ce projet, développé en TypeScript, propose une simulation d'aventure sur une carte, où des aventuriers évoluent, collectent des trésors et évitent des montagnes.

## Technologies Utilisées

- TypeScript
- Jest (pour les tests)

## Objectif du Projet

L'objectif principal est de créer une simulation d'aventure sur une carte, où les aventuriers peuvent se déplacer, collecter des trésors et éviter les obsctacles.

## Installation

1. Clonez le dépôt :

   ```bash
   git clone https://gitlab.com/bob53/carte-au-tresors.git
   ```

2. Accédez au répertoire du projet :

   ```bash
   cd '.\carte au trésors\'
   ```

3. Installez les dépendances:

   ```bash
    npm install
   ```

## Lancer le projet

Pour lancer l'application, utilisez la commande :

```bash
npm start chemin-fichier-entrée chemin-fichier-sortie
```

## Lancer les tests

Les tests sont écrits à l'aide de Jest. Pour exécuter les tests, utilisez la commande :

```bash
npm test
```

## Exemple d'utilisation

Voici un exemple de la simulation d'aventure sur la carte :

fichier en entrée :

```
C - 3 - 4
M - 1 - 0
M - 2 - 1
T - 0 - 3 - 2
T - 1 - 3 - 3
A - Lara - 1 - 1 - S - AADADAGGA
```

fichier en sortie :

```
C - 3 - 4
M - 1 - 0
M - 2 - 1
T - 1 - 3 - 2
A - Lara - 0 - 3 - S - 3
```
