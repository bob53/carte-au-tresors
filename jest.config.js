module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  testRegex: "(/src/.*\\.(test|spec))\\.[jt]sx?$",
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
};
